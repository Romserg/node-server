const express = require('express');
const router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.send('You are welcome to my first server');
});

module.exports = router;
