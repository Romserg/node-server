const express = require('express');
const router = express.Router();

const {createNewUser, getAllUsers, getUserById, removeUserById, updateUserById} = require('../services/user.service');

router.get('/', function (req, res, next) {
  const result = getAllUsers();
  res.send(result);
});

router.get('/:id', function (req, res, next) {
  if (
    req.params &&
    req.params.id &&
    !isNaN(parseInt(req.params.id))
  ) {
    const id = req.params.id;
    const result = getUserById(id);
    res.send(result);
  } else (
    res.status(400).send('Id must be numeric')
  )
});


router.post('/', function (req, res, next) {
  const data = {health, attack, defense} = req.body;

  const result = createNewUser(data);

  if (result) {
    res.send(`Created user: ${JSON.stringify(result)}`);
  } else {
    res.status(400).send('Wrong data for create');
  }
});

router.put('/:id', function (req, res, next) {
  if (
    req.params &&
    req.params.id &&
    !isNaN(parseInt(req.params.id))
  ) {
    const id = req.params.id;
    const data = {health, attack, defense} = req.body;
    data.id = id;
    const result = updateUserById(data);

    if (result) {
      res.send(`User updated: ${JSON.stringify(result)}`);
    } else {
      res.status(400).send('Id not found');
    }
  } else (
    res.status(400).send('Id must be numeric')
  )
});

router.delete('/:id', function (req, res, next) {
  if (
    req.params &&
    req.params.id &&
    !isNaN(parseInt(req.params.id))
  ) {
    const id = req.params.id;
    const result = removeUserById(id);
    if (result) {
      res.send(`Deleted user: ${JSON.stringify(result)}`);
    } else {
      res.status(400).send('Id not found');
    }
  } else (
    res.status(400).send('Id must be numeric')
  )
});

module.exports = router;
