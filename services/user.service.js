const {getAll, getUser, createUser, updateUser, removeUser} = require('../repositories/user.repository');


const getAllUsers = () => {
  return getAll();
};

const getUserById = (id) => {
  return getUser(id);
};

const createNewUser = (user) => {
  return createUser(user);
};

const removeUserById = (id) => {
  return removeUser(id);
};

const updateUserById = (user) => {
  return updateUser(user);
};

module.exports = {
  getAllUsers,
  getUserById,
  createNewUser,
  removeUserById,
  updateUserById
};
