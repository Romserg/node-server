const fs = require('fs');
const _ = require('lodash');
const {uniqueNamesGenerator} = require('unique-names-generator');

const refreshData = () => {
  return JSON.parse(fs.readFileSync('userlist.json'));
};


const createUser = (data) => {
  if (data) {
    const all = [...refreshData()];
    const user = {};
    user._id = String(all.length + 1);
    user.name = uniqueNamesGenerator(' ', true);
    Object.assign(user, data);
    all.push(user);
    fs.writeFileSync("userlist.json", JSON.stringify(all), function (err) {
        if (err) throw err;
      }
    );
    return user;
  } else {
    return false
  }
};

const getAll = () => {
  return refreshData();
};

const getUser = (id) => {
  return refreshData().find(item => item._id === id);
};

const updateUser = (user) => {
  if (user) {
    const {health, attack, defense} = user;
    const all = [...refreshData()];
    const indexForUpdate = _.findIndex(all, (item) => item._id === String(user.id));
    if (indexForUpdate !== -1) {
      all[indexForUpdate].health = health;
      all[indexForUpdate].attack = attack;
      all[indexForUpdate].defense = defense;
      fs.writeFileSync("userlist.json", JSON.stringify(all), function (err) {
          if (err) throw err;
        }
      );
      return user;
    } else {
      return false
    }
  } else {
    return false
  }
};

const removeUser = (id) => {
  const all = [...refreshData()];
  const indexForRemove = _.findIndex(all, (item) => item._id === String(id));
  if (indexForRemove !== -1) {
    const userForRemove = all[indexForRemove];
    all.splice(indexForRemove, 1);
    fs.writeFileSync("userlist.json", JSON.stringify(all), function (err) {
        if (err) throw err;
      }
    );
    return userForRemove;
  } else return false
};


module.exports = {
  createUser,
  getAll,
  getUser,
  updateUser,
  removeUser
};
